﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace ConsoleConnectOracleDB
{
    class Program
    {
        static void Main(string[] args)
        {
            
           string conString = "User Id=hr;Password=hr; Data Source=localhost:1521/orcl";
           OracleConnection con = new OracleConnection();
           con.ConnectionString = conString;

           try
           {
               con.Open();
               Console.WriteLine("connection success!");
               Console.ReadLine();
           }
           catch(OracleException ex)
           {
               Console.WriteLine(ex.ToString());
               Console.ReadLine();
           }


           OracleCommand cmd = con.CreateCommand();
           cmd.CommandText = "select first_name from employees where department_id = 90";

           OracleDataReader reader = cmd.ExecuteReader();

           while (reader.Read())
           {
               Console.WriteLine("employee Name: " + reader.GetString(0));
           }

           Console.WriteLine();
           Console.WriteLine("Press Enter");
           Console.ReadLine();
           
        }
    }
}
